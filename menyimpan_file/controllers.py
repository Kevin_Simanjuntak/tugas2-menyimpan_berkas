#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

from django.http import HttpResponse
from django.conf import settings
#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *
from login.OAuth import *

from rest_framework.exceptions import PermissionDenied

import os, shutil
import shutil
from zipfile import ZipFile
from django.http import HttpResponse

real_dir = os.getcwd()
class Examples(viewsets.ModelViewSet):
    serializer_class = PenyimpananSerializer
    queryset = Penyimpanan.objects.all()

    def list(self, request, *args, **kwargs):
        serializer = PenyimpananSerializer(self.get_queryset(), many=True)
        if is_authenticated(request):
            return Response({})
        else:
            return Response({"Message": "Silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        file = self.request.FILES.get('upload')
        if is_authenticated(request):
            if serializer.is_valid() :
                url = "http://" +request.get_host()+"/penyimpanan_file/File?filename="+str(file.name)
                penyimpanan = Penyimpanan.objects.create(upload = file, url = url)
                return Response({"url" : penyimpanan.url})
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        return Response({"Message": "silahkan login terlebih dahulu"}, status=status.HTTP_401_UNAUTHORIZED)

class GetFile(views.APIView):
    def get(self, request, format=None):
        filename = self.request.GET.get('filename', None)
        os.chdir(real_dir+'/Files')
        zip_file = open(filename, 'rb')
        response = HttpResponse(zip_file)
        response['Content-Disposition'] = 'attachment; filename='+ filename
        os.chdir(real_dir)
        return response