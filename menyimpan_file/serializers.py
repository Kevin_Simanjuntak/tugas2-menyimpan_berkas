#rest framework
from rest_framework import *
from rest_framework.serializers import *
from rest_framework.authtoken.models import *

#django
from django.contrib.auth.models import *

from .models import *

class PenyimpananSerializer(Serializer):
    upload = FileField()
    
    class Meta:
        model = Penyimpanan
        fields = "__all__"
        
