#rest_framework
from rest_framework.response import *
from rest_framework import status, views, viewsets, pagination, filters
from rest_framework import *
from datetime import datetime

#django
from django.db.models import Count
from django.contrib.auth.models import *
from django.contrib.contenttypes.models import *
from django.http import *

#3rd party
from django_filters.rest_framework import DjangoFilterBackend

#local
from .serializers import *
from .models import *
from .OAuth import *

from rest_framework.exceptions import PermissionDenied

class Examples(viewsets.ModelViewSet):
	serializer_class = login_serializer
	queryset = {}

	def create(self, request, *args, **kwargs):
		serializer = self.get_serializer(data=request.data)
		serializer.is_valid()
		username = serializer.data['username']
		password = serializer.data['password']
		token = get_token(username, password)
		if token == None :
			return Response({"Message": "username atau password yang anda berikan salah!"}, status=status.HTTP_401_UNAUTHORIZED)
		request.session['authorization'] = 'Bearer ' + token
		return Response({"Message": "silahkan menggunakan API kami."}, status=status.HTTP_202_ACCEPTED)



		
